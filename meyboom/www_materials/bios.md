## studio 1

### Overtoon

> <http://www.overtoon.org>

Overtoon is an artist-run platform for research, production and distribution of sound-based art. Artists can make use of a workspace in the centre of Brussels, production tools, technical expertise, artistic feedback, production admin, financial support and a broad international network.
Overtoon offers two types of residencies: production residencies and research residencies. While production residents are invited to deliver a work of art that Overtoon will present in its network, research residents can work in our studios in order to investigate an aspect that is related to sound. Artists can contact Overtoon for research residencies on a flexible basis. Production residencies are based on an ongoing dialogue with artists involved in the Overtoon network.

## studio 2

### Marialena Marouda

> <http://www.poetryexercises.de>

works in the intersections between performance and sound art. Her work is developed over the long term and is research-based. It takes place outside and beyong the black box, often developed in situ. She is currently the initiator of the Oceanographies Institute (2018-present), a research institute focusing on the relation between the human body and the ocean.

## studio 3

### Puxe

> <https://www.facebook.com/puxeaudio>

Puxe is a sound engineer, sound designer and DJ. Working and performing in live events, audio recording sessions, editing and mixing. He is currently developing *ArteFacto*, a project born to serve as a platform for promoting emerging artists through the organisation of events, concerts, performances and audiovisual projects.
As a DJ, he started off playing Hard House and Hard Trance in the late 90s' in local parties. Since he discovered Psychedelic Trance in 2004, it has been ever-present in his musical journey. He enjoys exploring sonic frequencies from Psytrance through progressive to downtempo.

## studio 5

### OSP

> <http://osp.kitchen>

Since 2006, Open Source Publishing questions the influence and affordance of digital tools through its practice of (commissioned) graphic design, pedagogy and applied research. They prefer to use exclusively free and open source softwares (F/LOSS). Currently the group is composed of people with backgrounds in graphic design, typography and development, and has has collaborated with organisations of different sizes, or individual artists. They find excitement in the cross-over between its members respective fields and competences.

### Amélie Dumont

> <https://www.amelie.tools>

Amélie is a graphic designer, typographer and developer. She cooks custom tools for publishing through webpages and print supports or designing fonts. Amélie also has experience in giving web-to-print workshops in art schools.

### Louis Garrido

> <https://www.instagram.com/louloularouille>

Louis is a graphic designer and typographer. He practices inclusive vector design in several forms: as part of publishing projects, identity commissions or self-publishing to be colored, on spherical shapes of petanque balls and through his detours in flea markets. Louis also drives workshops and interventions in art schools.


## studio 6

### Jubilee

> <http://jubilee-art.org>

Jubilee is an artist-run platform for artistic research that continuously supports 4 core artist practices, as well as various associated artists on a project basis. Jubilee helps artists develop their work and organises collective research projects that facilitate interdisciplinary and cross-sectoral reflection on shared interests. The conditions of artistic production form a recurring thread.

## studio 7
### Stijn Demeulenaere

> <http://stijndemeulenaere.be>

Stijn Demeulenaere is a sound artist and field recordist. He creates installations, soundscapes, performances, and films. He collaborates with choreographers theatre and film makers; visual artists, scientists and musicians. Stijn researches the relation between place, identity, sound, and the phenomenology of listening. His work is shown internationally, has won prizes at the Engine Room International Sound Art Award and the Split Videoart Festival, and was nominated for the LOOP Discover award and the European Sound Art Award.


### Ioana Mandrescu

> <https://www.ioanamandrescu.com>

Romanian born sound artist and musician Ioana Mandrescu completed her studies in her home country, France and Belgium. She is graduate of the EPAS programme in Ghent.
She is the founding member of two contemporary music ensembles.
Strongly influenced by choreography and painting, in the past two years she developed performances that brought together sound, film and dance. These were presented internationally and mark the beginning of her development as a sound artist, next to a classical musician.
Her main interests are the human voice and human produced sounds and their impact on the way we perceive the world. She is currently working on a database of human voices and on several sound-collage pieces that mark distinct moments in human history. Another strong line of research is the human body, starting with a project about her own heart coming in 2022. Ioana lives and works in Brussels.



## studio 8
## North*East

### Marilyne Grimmer

> <http://www.marilynegrimmer.net>

is a French visual artist and sometimes a scenographer based in Brussels since 2008.
She alternates between personal projects as a photographer, participatory projects with diverse audiences, collaboration as a scenographer for theatre companies, installations and performances with the Repondeur automatik collective.
In her personal work, she likes to visit strangers to hear their stories, everyday stories, object's stories, travel stories. Sometimes she teleportate them to the place of their dreams.
She often uses the public space to share the stories she collected (in advertisement board, window shop..) so that they are accessible by anyone who passes by.

### Daniel de Almeida Cabral

> <https://www.danielcabral.org>

(°1989, Recife) is a multimedia visual artist from Brazil based in Brussels since 2015. After completing a Bachelor's degree in International Relations, he earned a Master’s degree in Visual Arts from Sint-Lucas School of Arts Antwerp, Belgium (2020). Cabral has a contextual practice that responds to his surroundings through a wide range of mediums, including drawing, collage, photography, video, installations,  and performances. His practice is based on three main pillars: the male body and the representation of masculinities (in particular concerning gay culture); speech, inspired by a background as public speaker in International Relations; and lastly, the city and the sense of displacement, initiated since he immigrated from Brazil to Belgium in 2015.


### Mara Ittel

> <https://www.mmittel.com>

is an artist, editor and a pop culture expert. She is exploring obsessions, relationships and queer fandoms through making films and interviews, for example for her (german) podcast *Catch a Fan Feeling*. She is also part of creating the website BAD.brussels, a place to collect and map queer and feminist spaces and collectives in Brussels.

### Ingrid Vranken

Ingrid Vranken (°1987) works as an independent dramaturg, curator and artist, and is a member of FoAM, a transdisciplinary laboratory at the interstices of art, science, nature and everyday life. The collaborative project Common Wallet has shaped her daily life since 2018, a life-experiment sharing finances with nine other people as a way to rethink individual ownership, the commons, precarity and affective relations to money. Her curatorial and artistic practice focuses on enabling a systemic ecofeminist transition in the arts, through engaging with the knowledge and labor of other-than-human beings, and in particular plants and ghosts. Since the spring of 2020 she is part of the multi-voiced curatorial team of wpZimmer, a space for artistic development in Antwerp.

### Christina Stadlbauer

> <http://christallinarox.wordpress.com>

Works with plants, animals, bacteria at the Institute for Relocation of Biodiversity to find unorthodox solutions. Keeps bees at Melliferopolis, and engages with cracks and the ancient Japanese craft of mending them with gold - KinTsugi.


### Samah Hijawi

> <https://www.samahhijawi.com>

Samah Hijawi is an artist and researcher currently doing her PhD in Art Practice at ULB and the Academie Royale Des Beaux Art de Bruxelles, Belgium. In 2020 she started a long term project Kitchen. Table. that researches the movement of food practices over time and across geographies, and the body as a site of food memory. Her earlier work focused on unpacking the political gesture in artistic works that allude to the histories of Palestine, a research that manifested in a series of collages and lecture performances from 2017-2019. The essence of the question of where the political is embedded in artistic work, continues in the on-going series Aesthetics of the Political, a project that involves creating artist to artist encounters to explore how political ideas are transformed to artistic form.

###Bárbara C. Branco

> <https://www.barbaracbranco.pt>

My practice is focused in inventing codes to discern little happenings in life, mapping them in a way this little thing becomes maybe for the first time the focus of our attention. The empirical aspect of my approach is fundamentally vital, and often site specific. Be it the wind, or a dropping leave, or a drop of rain, everyday magic is real.

###Lucia Palladino

###Gosie Vervloessem

## studio 9

### aRzu

> <https://arsound.studio>

Sound designer and sound artist. working in animation film as well as various audio visual projects across disciplines, boarders and spaces.

## studio 10

### Fusion Cinema

> <http://www.pagesofalbum.com>

Film was labelled "the seventh art" at the begin of the XXth century after a manifesto by Riciotto Canudo, one of first film theoretitians. He saw first cinema as a new art, a "fusion" art: "a superb conciliation of the Rhythms of Space and the Rhythms of Time". Fusion Cinema is small-scale non profit platform devoted to film research and production to further explore the fusion of arts whithin cinema. Its flagship project, "Pages of Album" is signed by filmmakers Erik Parys and Pablo Diartinez (also plastic artist and graphic designer). Erik and Pablo have been a creative tandem since 1999 and they also have a commercial name "C U studio" under which they take comissions for better known agencies. Their commercial work span from videoclips, advertising spots, special effects and motion graphics for broadcast or social media to illustration, lay-out, logos and identity systems for print or web.

### Eyas Al Mokdad

A Syrian Belgian filmmaker and choreographer, Al Mokdad holds a Master in Audiovisual Arts (Film) from The Royal Academy of Fine Arts (KASK) 2021,  an Advanced Master Degree in Audiovisual Arts from LUCA school of arts Brussels 2013, a bachelor's degree from the Higher Institute of Dramatic Arts (Ballet Department) Damascus 2005.
Al Mokdad's work focuses on experimentation, to explore contemporary forms of art in cinema and performing arts. In recent years, he has worked on a number of films and performances that deal with political and social issues. He is a researcher in contemporary education and the use of art in the development of abilities and skills.

## studio 11

### Sarah van Lamsweerde

> <http://www.sarahvanlamsweerde.com>

creates performances, installations, and publications that focus on oral traditions and tactility to reflect on memory, co-existence and otherness. Alongside her own work, she contributes to practices and platforms such as Performing Objects (ERforS, Brussels), BAU Dance & Performance (Amsterdam), and IM Company (Paris).

### Antje


## studio 12

### Clara Sambot

is a member of the [ByeByeBinary collective](http://genderfluid.space). She is a student at ERG (Brussels) and will soon finish her master's degree in design and politics of the multiple, with a typography option. Clara publishes in May 2020 the typeface Cirrus Cumulus with inclusive glyphs on the [Velvetyne foundry](http://velvetyne.fr/fonts/cirruscumulus). Her new typeface DINdong, is a revival of the DIN 1451 (still in progress). Clara is also currently working on building an online library of fonts with inclusive glyphs under a free license.  Her researches turn around inclusivity in typography and the links that can be drawn with the open source and free license.

### Marouchka Payen

has a master's degree in typography from ERG, is a graphic designer and lettering hunter by day, and a DJ by night. She is part of the Belgian collective [Poxcat](https://www.poxcat.com) which promotes women in music by organizing events and hosting a monthly radio show on [The Word Radio](https://theword.radio/residents/marouchka). Through her practice of lettering and her interest in digital typographic drawing through the skeleton, she brings a particular care to the ligatures that she melts into alloys as many transitions and passages within her mixes. Marouchka is also a member of the ByeByeBinary collective.

### Manon Didierjean

student at ERG in Master Narration Speculative. Her writing work takes several narrative forms (novel, poetry and theory), and questions the control of bodies and their imposed normalities. Her research is notably in relation with cinematographic images, in a feminist approach linked to cultural studies, and questions the imaginary that we construct, between individual and collective projections, intimate and political. She currently works at the Tulitu bookstore, which is linked to her interest in publishing.

### Chloe Horta

former ERG student with a master degree in Design & Politique du Multiple (2020), works with prints, graphic matters and bookbinding. She published a book called *Comment survivre après l'école d'art ?* with Surfaces Utiles. She also holds a bachelor from 75 (2018) and kept a silkscreen practice from there. Currently working on a more ecological and DIY-open source silkscreen tools and also inclusive arrow word puzzles.

### Thamara Hidalgo

born in Caracas in 1995. Of Franco-Italian-Venezuelan origin, I migrated to Europe in 2001. Arrived in Brussels in 2014, and graduated in graphic design,  and illustration, I just finish a master in Design and Politics of the Multiple (and typography) at ERG. I'm interested in issues of migration, postmigration, belonging, memory and cultural hybridity. I work in various formats including photography, video, publishing and textile design.

### Shirine Abel

Has a bachelor degree in photography from Le75 as well as a master degree in Speculative Narration (option videography) from ERG.
She has done several projects on the topic of urbanism, usually choosing to work on one specific detail or material (like the "bluestone") to explore different nods, always balencing the political and poetical.
She is currently working on a new film about the "arabité", through the jasmin plant and the learning of arabic language, she tries to dissect her own story and what can be transmitted from body to body (from generation to generation) linking the memories trough sounds, smells and gestures. Anchering it in a more global story about arabic diapsora, she explores the trauma of police violence and tries to make a decolonial and anti-racist narrative.

### Simon Bouvier

ERG student with a master degree in Design & Politique du Multiple (2021), currently working on his thesis. His projects revolve around working with open source web tools, seeing web design as a playfull and narrative platform, and navigating typography in a different way.
