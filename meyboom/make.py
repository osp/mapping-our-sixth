
from jinja2 import Template
from markdown import markdown
from markdown3_newtab import NewTabExtension

SVG_PATH = 'www_materials/plan.svg'
BIOS_PATH = 'www_materials/bios.md'
INTRO_PATH = 'www_materials/intro.md'
TEMPLATE_PATH = 'www_materials/template.html'
HTML_PATH = 'www/index.html'


extensions = ['toc', 'markdown3_newtab']

configs = {
    'toc': {
        'permalink': '&#10687;'
    }
}

def md_parsing(NAME_PATH):
    md = ""
    with open(NAME_PATH, 'r') as file:
        md = markdown(file.read(), extensions=extensions, extension_configs=configs)
    return md

if __name__ == '__main__':

    # getting svg
    with open(SVG_PATH, 'r') as file:
        svg = file.read()

    # getting markdown
    bios = md_parsing(BIOS_PATH)
    intro = md_parsing(INTRO_PATH)

    # getting the template
    with open(TEMPLATE_PATH, 'r') as file:
        template = Template(file.read())

    html = template.render(svg = svg, bios = bios, intro = intro)
    with open(HTML_PATH, 'w') as file:
        file.write(html)
