
# Simple Interactive SVG Map

The idea is to have
1. a map of the floor which is an **SVG**
2. short descriptions for every studio of the floor in a **markdown** file
3. an **html** file containing both the map and the description, generated in **python** with the two previous files
4. **interactivity**
    * hovering on a room create a visual effect to show that there is interactivity
    * clicking on a room of the map autoscroll to the corresponding studio description

All of this without any javascript :-)

## Crafting the SVG

As this SVG is interactive and styled in css, it requiers to care about it's internal structure such as
* classes and id's
* element nesting

### SVG structure

```XML
<!-- the outlines -->
<g id="walls">
├── <path>
├── <path>
├── ...
└── ...

<!-- every group of element inside is a studio -->
<g id="rooms">
|   <!-- the link to the bios header anchor -->
├── <a class="room" id="svg__studio-1" href="#studio-1">
|     |   <!-- a path of the studio surface -->
|     ├── <path class="surface">
|     └── <text class="studioID">
├── <a class="room" id="svg__studio-2" href="#studio-2">
├── ...
└── ...
```

Those needs to be satisfied for it to work
* the outlines have to be first so they appear on top of the `.surface` elements.
* the anchor links contains the `.surface` element (so they are triggered when we click anywhere on the contained `path`)
* because both `.surface` and `.studioID` need to be descendant of the `.room` group (so we can create a `:hover` effect on the `room`in CSS)
* the `href` of the `a` elements are the header title with spaces replaced with a minus dash

Also in order to use CSS on a SVG, in a separate sheet we need
    * that the SVG is hardcoded in the html file (inserted with python or copy pasted)
    * that inline-style have been removed from element (or else it is going to have higher priority)

### Inkscape + IDE crafting

You can create such an SVG using Inkscape with:
* the object pannel opened, to look and interact with the nesting
* the CSS pannel opened, to clear inline styles and assign classes or id's

Still it's sometimes a bit tricky to do exactly what you want... I often close the SVG in Inkscape and open it in my IDE to do search and replace (on classes name, or inline-style for example) then open it back in Inkscape. It's a bit tedious but that's part of the craft.
With those tools we can draw the outlines layer and then the group for every studio (assigning the class: `.room`) with two child: the `.surfaces` `path`, and the `.sudioID` `text`, from which we clear the inline-style. Something that helps is to create one group with proper classes and inline-style cleaned and then copy-paste this one and edit it for every other elements with the same status.
Then we have to create the `a` element by hand, opening the SVG in our IDE and search for `class="surface"`, and manually write the `href`. Note that we could do it in python, but it was honestly faster to do it by hand for 12 elements.


## python templating with jinja and markdown

Because we have to put the map has a `<svg>` in the HTML, and we have a separate markdown witht the content, it makes sense to use templating so we don't have to directly interact with this super long, hard to read, html file.

The python is quite simple and we can read it directly in `make.py` file.
The two important things is that first we separated in the folders the materials (`www_materials` folder) and the rendered HTML plus the CSS files (`www` folder). It makes it a bit easier to put it online because we can just copy the whole `ẁww` folder.

The second thing is that we use the [markdown.extensions.toc](https://python-markdown.github.io/extensions/toc/) along with a small configs json variable, in order to automatically creates `id` to the header. Those `id` are necessary in order to make the autoscroll interactivity works. In those header id the spaces are going to be replaced by a minus dash, so we have to create the `href` of the anchor inside the map accordingly.

The browser does the rest of the magic, as going to a url like so `http://mywebsite.com/index.html#myid` is automatically going to scroll on the page until the element with `id=myid` is in view.

To generate to webpage we do

    python3 make.py


It's a matter of preference, but a thing we can now do in CSS is to use [smooth-behavior](https://developer.mozilla.org/en-US/docs/Web/CSS/scroll-behavior), so we have a continuous scrolling transitions while clicking on the different rooms.

```CSS
html{
  scroll-behavior: smooth;
}
```

## Super cool responsive CSS styling on an SVG

The fact that the SVG is an actual SVG element in the document, and that it has been emptied of it's inline styles, give us full dynamic control of it's look through CSS.

This open to SVG / HTML manipulation in a total new way, like they're both clays of different color and you're creating a marbled sculpture, instead of simply sticking in SVG balls in the HTML structure.

### Custom Properties

We can use CSS custom-properties that are shared by HTML element, like colors!
Note that the SVG properties are not the same than the one on HTML elements.

```CSS
:root{
  --stroke: black;
  --surface: blue;
}

/* svg */
path.walls{
  fill: none;
  stroke: var(--stroke);
}
path.surface {
  fill: var(--surface);
  fill-opacity: 0.06;
}

/* html */
div.box{
  background: var(--surface);
  opacity: 0.06;
}
```

### Global stroke-width units

Originally, the `stroke-width` we are defining for the SVG elements are relative to the `viewbow` of the SVG. Which means that it's going to scale proportionnally with the SVG size, and that it doesn't care about if it's actually `1px` in the webpage. The unit system is intern to every SVG on your webpage, and does not synchronise by default.

We can change this using the [vector-effect](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/vector-effect) property. By combining with the previous point, we can make it matchs other element on our webpage like the borders, or even the underlines fo the links.

```CSS
:root{
  --stroke: blue;
  --stroke-width: 1.5px;
}

/* svg */
path.walls{
  vector-effect: non-scaling-stroke;
  stroke: var(--stroke);
  stroke-width: var(--stroke-width);
}

/* html */
div.box{
  border: var(--stroke-width) solid var(--stroke);
}
a{
  text-decoration-color: var(--stroke);
  text-decoration-thickness: var(--stroke-width);
}
```

### Hover effect

```CSS
.room .surface {
  fill: transparent;
}

.room:hover .surface {
  fill: blue;
}
.room:hover .studioID {
  font-style: italic;
}
```

### Using the same font

We can directly use the same font inside of our SVG.

```CSS
text.studioID {
  font-family: 'myfont';
  font-size: 13px;
  letter-spacing: 0px;
  line-height: 1.25;
}
```

**However there is currently no property to have a global `font-size` for SVG text elements** in the same way than for the `stroke-width`.

### Media queries

We can do media query for more responsiveness.
It's possible that the SVG is going to look a lot smaller on our phones. If it contains text we may want to boost a bit the font-size of this text like so:

```CSS
text.studioID {
  font-family: serif;
  font-size: 12px;
}

/* mobile */
@media only screen and (max-width: 600px) {
  text.studioID {
    font-size: 20px;
  }
}
```

### Pointer-events

We can add the CSS property `pointer-events: none;` to element that we want to be able to click through (for example text that goes on top of different rooms).
