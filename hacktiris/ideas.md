
Faire attention:
* quand on crée / copie-colle un éléments dans inkscape, le placer dans le bonne embrenchement d'objet pour que les classes / hover s'applique (voir panneau objets)
* quand on crée / copie-colle un éléments dans inkscape, supprimer son style inline pour que les classes / hover s'applique (voir panneau xml editor)

## Web Version

It could be fun to make a **webpage that is only an svg**.

By playing with **css classes in the svg** we can produce **hover effect** on the interactive element of the map.

Open "webversion.svg" in your browser to test it.
Click on the OSP room it should be a link to OSP website.

ideas:

* The ellipsis doors could make a little animation (turn, swosh, grow,...) when any part of the room is hoovered.
* the different rooms are links to the organisation websites.


**IMPORTANT NOTES:**
* The hover effect works in a webbrowser.
* The hover effect works only if the fill color of the room is different than "none": it has to be filled with something so when your cursor is inside a room it is actually "on" the svg element. I put the fill color of the rooms to "transparent" and it worked, **BUT** inkscape doesn't understand the "transparent" word and will fill the rooms with black. So **if you want to open it in inkscape,** first open it in a text editor and change the fill property of the class .room (in the style section) from "transparent" to "none".
* The hover effect works with css classes applied on groups of elements, if you modify, copy or ungroup those elements it is possible that they looses their class attribute or got some inline svg styling added, you have to be carrefull.

## Design idea

i like the idea that the map is like a **video game** map, the pictograms indicating where and how you can **interact** with things.

* emphazise on **doors** as little portals: ellipsis or spirals(?). doors in video games are interactive object (you can click on it) and they make a transition between two spaces, often with a little animation.
* only put those doors for the occupied rooms. these anchor points for interactivity make it directly clear on the map what is occupied and what is not, and where you can go. Adding new doors later would feels like unlocking something.  
* the enter/exit points: the elevators / stairs. Those would also be accompagned of a little picto: two-sided up-down arrows ?
* a particular door / ellipsis / picto for special spot like the common space, kitchen or wc.
* special ellipsis for the badge-locked doors(?)


## Plotted Version

* trace the architure with a grey(?) pen in a very standard-clean-robotic way.
* trace the elipsis with a **brush-pen** (i have one in particular) to have a more variable stroke. It should look a bit like **someone quickly circled/marked** the occupied zone with a big pen over the map.
* try the hershey fonts with this brush-pen for the name, maybe slanted(?) to give it a mixed feel between machine gesture and hand gesture.
